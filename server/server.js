// Base setup
// ==========

// dependencies
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// configure app to use bodyParser() to get the data from a POST
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

var port = process.env.PORT || 8888;

// API ROUTES
// ==========

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'Het werkt!'});
});

// More routes go here

// Register routes
// Prefix all routes with /api
app.use('/api', router);

// START THE SERVER
// ================
app.listen(port);
console.log('Server is listening on port '+port);